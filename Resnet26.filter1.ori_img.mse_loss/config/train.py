import os
import sys
import cv2
import argparse
import numpy as np
import itertools

import torch
from torch import nn
from torch.nn import MSELoss, DataParallel
from torch.optim import Adam, SGD
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter

import settings
from dataset import DerainDataset 
from model import DetailNet

logger = settings.logger
torch.cuda.manual_seed_all(66)
torch.manual_seed(66)
torch.cuda.set_device(settings.device_id)


def ensure_dir(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
        

class Session:
    def __init__(self):
        self.log_dir = settings.log_dir
        self.model_dir = settings.model_dir
        ensure_dir(settings.log_dir)
        ensure_dir(settings.model_dir)
        logger.info('set log dir as %s' % settings.log_dir)
        logger.info('set model dir as %s' % settings.model_dir)

        self.net = DetailNet()
        if settings.num_GPU > 1:
            self.net = DataParallel(self.net, 
                    device_ids=list(range(settings.num_GPU)))
        else:
            self.net = self.net.cuda()
        self.crit = MSELoss().cuda()
        self.opt = Adam(self.net.parameters(), lr=settings.lr)
        #self.opt = SGD(self.net.parameters(), lr=settings.lr, momentum=0.9, weight_decay=1e-10)

        self.step = 0
        self.save_steps = settings.save_steps
        self.num_workers = settings.num_workers
        self.batch_size = settings.batch_size
        self.writers = {}
        self.dataloaders = {}

    def tensorboard(self, name):
        self.writers[name] = SummaryWriter(os.path.join(self.log_dir, name + '.events'))
        return self.writers[name]

    def start(self):
        self.save_checkpoints('latest')

    def write(self, name, out):
        for k, v in out.items():
            self.writers[name].add_scalar(k, v, self.step)

        out['step'] = self.step
        outputs = [
            "{}:{:.4g}".format(k, v) 
            for k, v in out.items()
        ]
        logger.info(' '.join(outputs))

    def get_dataloader(self, dataset_name, use_iter=True):
        dataset = DerainDataset(dataset_name)
        self.dataloaders[dataset_name] = \
                    DataLoader(dataset, batch_size=self.batch_size, 
                            shuffle=True, num_workers=self.num_workers, drop_last=True)
        if use_iter:
            return itertools.cycle(self.dataloaders[dataset_name])
        else:
            return self.dataloaders[dataset_name]

    def save_checkpoints(self, name):
        ckp_path = os.path.join(self.model_dir, name)
        obj = {
            'network': self.net.state_dict(),
            #'optim': self.opt.state_dict(),
            'clock': self.step,
        }
        torch.save(obj, ckp_path)

    def load_checkpoints(self, name):
        ckp_path = os.path.join(self.model_dir, name)
        try:
            obj = torch.load(ckp_path)
        except FileNotFoundError:
            return
        self.net.load_state_dict(obj['network'])
        #self.opt.load_state_dict(obj['optim'])
        self.step = obj['clock']

    def inf_batch(self, name, batch):
        data, label = batch['data'], batch['label']
        data, label = data.cuda(), label.cuda()
        data, label = Variable(data), Variable(label)

        pred = self.net(data)
        loss = self.crit(pred, label)

        return pred, loss

    def save_image(self, name, img_lists):
        return
        data, pred, label, filted = img_lists
        pred = pred.data.cpu().numpy()
        data, pred, label, filted = data * 255, pred * 255, label * 255, filted * 255
        data, pred, label = data + filted, pred + filted, label + filted
        pred = np.clip(pred, 0, 255)
        h, w = pred.shape[-2:]

        gen_num = (12, 4)
        img = np.zeros((gen_num[0] * h, gen_num[1] * 3 * w, 3))
        for img_list in img_lists[:3]:
            for i in range(gen_num[0]):
                row = i * h
                for j in range(gen_num[1]):
                    idx = i * gen_num[1] + j
                    tmp_list = [data[idx], pred[idx], label[idx]]
                    for k in range(3):
                        col = (j * 3 + k) * w
                        tmp = np.transpose(tmp_list[k], (1, 2, 0))
                        img[row: row+h, col: col+w] = tmp 

        img_file = os.path.join(self.log_dir, '%d_%s_%d.jpg' % (self.step, name, i))
        cv2.imwrite(img_file, img)


def run_train_val(ckp_name='latest'):
    sess = Session()
    sess.load_checkpoints(ckp_name)

    sess.tensorboard('train')
    sess.tensorboard('val')

    dt_train = sess.get_dataloader('train')
    dt_val = sess.get_dataloader('val')

    while True:
        sess.net.train(True)
        sess.net.zero_grad()
        batch_t = next(dt_train)
        pred_t, loss_t = sess.inf_batch('train', batch_t)
        sess.write('train', {'loss': loss_t.data[0]})
        loss_t.backward()
        sess.opt.step()

        sess.net.train(False)
        batch_v = next(dt_val)
        pred_v, loss_v = sess.inf_batch('val', batch_v)
        sess.write('val', {'loss': loss_v.data[0]})

        if sess.step % int(sess.save_steps / 4) == 0:
            sess.save_checkpoints('latest')
        if sess.step % int(sess.save_steps / 2) == 0:
            sess.save_image('train', [batch_t['data'], pred_t, batch_t['label'], batch_t['filted']])
            sess.save_image('val', [batch_v['data'], pred_v, batch_v['label'], batch_v['filted']])
            logger.info('save image as step_%d' % sess.step)
        if sess.step % sess.save_steps == 0:
            sess.save_checkpoints('step_%d' % sess.step)
            logger.info('save model as step_%d' % sess.step)
        sess.step += 1


def run_test(ckp_name):
    sess = Session()
    sess.net.train(False)
    sess.load_checkpoints(ckp_name)

    dt = sess.get_dataloader('test_syn', use_iter=False)

    all_num = 0
    all_mse = 0.
    for i, batch in enumerate(dt):
        pred, loss = sess.inf_batch('test', batch)
        sess.save_image('test', [batch['data'], pred, batch['label'], batch['filted']])
        logger.info('batch %d mse loss: %f' % (i, loss.data[0]))

        batch_num = pred.size(0)
        all_num += batch_num
        all_mse += batch_num * loss.data[0]

    logger.info('total mse loss: %f' % (all_mse / all_num))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--action', default='train')
    parser.add_argument('-m', '--model', default='latest')

    args = parser.parse_args(sys.argv[1:])
    
    if args.action == 'train':
        run_train_val(args.model)
    elif args.action == 'test':
        run_test(args.model)

