import os
import logging


batch_size = 128
patch_size = 64 
lr = 1e-5

data_dir = '/data/Datasets/lixia/Rain800'
filt_dir = '/data/Datasets/lixia/filtered_16'
log_dir = '../logdir'
model_dir = '../models'

log_level = 'info'
model_path = os.path.join(model_dir, 'latest')
save_steps = 100

num_workers = 16
num_GPU = 1
device_id = 0

logger = logging.getLogger('train')
logger.setLevel(logging.INFO)

fh = logging.FileHandler(os.path.join(log_dir, 'train.log'))
fh.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)


