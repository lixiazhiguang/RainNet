import os
import cv2
import numpy as np
from numpy.random import RandomState
from torch.utils.data import Dataset

import settings 


class DerainDataset(Dataset):
    def __init__(self, name):
        super().__init__()
        self.rand_state = RandomState(66)
        self.root_dir = os.path.join(settings.data_dir, name)
        self.filt_dir = os.path.join(settings.filt_dir, name)
        self.mat_files = os.listdir(self.root_dir)
        self.patch_size = settings.patch_size
        self.file_num = len(self.mat_files)

    def __len__(self):
        return self.file_num * 100

    def __getitem__(self, idx):
        file_name = self.mat_files[idx % self.file_num]
        img_file = os.path.join(self.root_dir, file_name)
        fil_file = os.path.join(self.filt_dir, file_name)

        img_pair = cv2.imread(img_file).astype(np.float32) / 255
        filt_img = cv2.imread(fil_file).astype(np.float32) / 255
        img_pair[:, :, 0], img_pair[:, :, 2] = img_pair[:, :, 2], img_pair[:, :, 0]
        filt_img[:, :, 0], filt_img[:, :, 2] = filt_img[:, :, 2], filt_img[:, :, 0]
        h, w, c = img_pair.shape
        patch_size = self.patch_size

        half_w = int(w / 2)
        r = self.rand_state.randint(0, h - patch_size)
        c = self.rand_state.randint(0, half_w - patch_size)

        data = img_pair[r: r+patch_size, c+half_w: c+patch_size+half_w]
        label = img_pair[r: r+patch_size, c: c+patch_size]
        filted = filt_img[r: r+patch_size, c: c+patch_size]
        label = data - label
        data = np.concatenate([data, data - filted], axis=2)

        data = np.transpose(data, (2, 0, 1))
        label = np.transpose(label, (2, 0, 1))
        filted = np.transpose(filted, (2, 0, 1))
        sample = {'data': data, 'label': label, 'filted': filted}

        return sample


if __name__ == '__main__':
    dt = DerainDataset('train')
    smp = dt[100]
    data = smp['data']
    label = smp['label']
    print(data.shape, label.shape)
