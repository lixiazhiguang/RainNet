import os
import cv2
import numpy as np
from numpy.random import RandomState
from torch.utils.data import Dataset

import settings 


class DerainDataset(Dataset):
    def __init__(self, name):
        super().__init__()
        self.rand_state = RandomState(66)
        self.root_dir = os.path.join(settings.data_dir, name)
        self.mat_files = os.listdir(self.root_dir)
        self.patch_size = settings.patch_size
        self.file_num = len(self.mat_files)

    def __len__(self):
        return self.file_num * 100

    def __getitem__(self, idx):
        file_name = self.mat_files[idx % self.file_num]
        img_file = os.path.join(self.root_dir, file_name)
        img_pair = cv2.imread(img_file).astype(np.float32) / 255
        h, w, c = img_pair.shape
        patch_size = self.patch_size

        half_w = int(w / 2)
        r = self.rand_state.randint(0, h - 1 - patch_size)
        c = self.rand_state.randint(0, half_w - 1 - patch_size)
        data = img_pair[r: r+patch_size, c+half_w: c+patch_size+half_w]
        label = img_pair[r: r+patch_size, c: c+patch_size]

        gray = cv2.cvtColor(img_pair[r: r+patch_size+1, c+half_w: c+patch_size+half_w+1], cv2.COLOR_BGR2GRAY)[:, :, np.newaxis]
        grad_x = gray[:-1, :-1] - gray[:-1, 1:]
        grad_y = gray[:-1, :-1] - gray[1:, :-1]

        label = data - label
        data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
        label = cv2.cvtColor(label, cv2.COLOR_BGR2RGB)
        data = np.concatenate([data, grad_x, grad_y], axis=2)

        data = np.transpose(data, (2, 0, 1))
        label = np.transpose(label, (2, 0, 1))
        sample = {'data': data, 'label': label}

        return sample


if __name__ == '__main__':
    dt = DerainDataset('train')
    smp = dt[1]
    data = smp['data']
    label = smp['label']
    print(data.shape, label.shape)
