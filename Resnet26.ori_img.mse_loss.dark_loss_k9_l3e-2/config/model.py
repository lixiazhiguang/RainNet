import torch
from torch import nn
from torch import autograd
from torch.autograd import Variable
from torch.nn import functional as F


class SEBlock(nn.Module):
    def __init__(self, input_dim, reduction):
        super().__init__()
        mid = int(input_dim / reduction)
        self.avg_pool = nn.AdaptiveAvgPool1d(1)
        self.fc = nn.Sequential(
            nn.Linear(input_dim, reduction),
            nn.ReLU(inplace=True),
            nn.Linear(reduction, input_dim),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1)
        return x * y


class ConvBNRelu(nn.Module):
    def __init__(self, inp_dim, oup_dim, use_relu=True):
        super().__init__()
        self.block = nn.Sequential(
            nn.Conv2d(inp_dim, oup_dim, 3, padding=1),
            nn.BatchNorm2d(oup_dim),
            nn.ReLU()
        ) if use_relu else nn.Sequential(
            nn.Conv2d(inp_dim, oup_dim, 3, padding=1),
            nn.BatchNorm2d(oup_dim)
        )

    def forward(self, x):
        x = self.block(x)
        return x


class ResBlock(nn.Module):
    def __init__(self, inp_dim, oup_dim):
        super().__init__()
        self.res_block = nn.Sequential(
            ConvBNRelu(inp_dim, oup_dim),
            ConvBNRelu(inp_dim, oup_dim)
        )

    def forward(self, x):
        res = self.res_block(x)
        res += x
        return res


class DetailNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = ConvBNRelu(3, 16)
        self.convmids = nn.ModuleList([
            ResBlock(16, 16)
            for i in range(12)
        ])
        self.conv26 = ConvBNRelu(16, 3, use_relu=False)

    def forward(self, x):
        x = self.conv1(x)
        for i, conv in enumerate(self.convmids):
            x = conv(x)
        x = self.conv26(x)
        return x 


class DarkLayer(nn.Module):
    def __init__(self):
        super().__init__()
        self.dark = nn.MaxPool2d(9, stride=1, padding=4)

    def forward(self, x):
        x, idx = x.min(dim=1, keepdim=True)
        x = -self.dark(-x)
        return x#, idx


if __name__ == '__main__':
    ts = torch.Tensor(16, 3, 64, 64)
    data = Variable(ts)

    net = DetailNet()
    layer = DarkLayer()

    pred = net(data)
    dark_data, dark_idx = layer(data)
    pred_dark = layer(pred)
    print(pred_dark.size())


